<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TUGAS AKHIR</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
</head>
<body>
	<div class="medsos">
		<div class="container">
			<ul>
				<li><a href="https://www.facebook.com/alviandri.yusuf"><i class="fa-brands fa-facebook"></i></a></li>
				<li><a href="https://www.instagram.com/alvadrysf_/?hl=id"><i class="fa-brands fa-instagram-square"></i></a></li>
				<li><a href="https://twitter.com/home"><i class="fa-brands fa-twitter-square"></i></a></li>
			</ul>
		</div>
	</div>
	<header>
		<div class="container">
			<h1><a href="PWeb2022-Tugas05-SKRIP-G-2100018321-Alviandri Yusuf.html">Alviandri Yusuf</a></h1>
			<ul>
				<li class="active"><a href="PWeb2022-Tugas06-SKRIP-G-2100018321-Alviandri Yusuf.html">HOME</a></li>
				<li><a href="#ABOUT">ABOUT</a></li>
				<li><a href="#Image">IMAGE</a></li>
				<li><a href="#contact">CONTACT</a></li>
				<li><a href="#Contact us">MASSAGE</a></li>
			</ul>
		</div>
	</header>

	<section class="benner">
		<h2>WEB PROFILE</h2>
	</section>

	<section class="home">
		<div class="container">
				<div class="home-left">
					<img src="foto1.jpg">
					<h2>Halo..<br>
						PERKENALKAN  SAYA ALVIANDRI YUSUF<span class="efek-ketik"></span></h2>
					<p>Selamat datang di website saya!</p> <span class="efek-ketik2"></span>
				</div>
			</div>
	</section>
	<section id="login">
            <div class="home">
            	<div class="container">
            		<h3>Form Daftar kunjungan</h3>
            		<h4>Mohon untuk mengisi form daftar kunjungan di bawah ini: </h4>
                	<div class="home-section">
                    <div class="login1">
                    <form action="proses.php" method="POST" onsubmit="validasi()">
            <div>
                <label>
                    Nama Lengkap:
                </label>
                <input type="text" name="nama" id="nama">
            </div>
            <div>
                <label>
                    Email:
                </label>
                <input type="email" name="email" id="email">
            </div>
            <div>
                <label>
                    Website:
                </label>
                <input type="website" name="website" id="website">
            </div>
             <div>
                <label>
                    Status:
                </label>
                <select name="status" id="status">
                	<option>Pelajar</option>
                	<option>Single</option>
                	<option>Menikah</option>
                </select>
            </div>
            <div>
                <label>
                    Alamat:
                </label>
                <textarea cols="40" rows="5" name="alamat" id="alamat">
                </textarea>
            </div>
            <div>
                <input type="submit" value="Login" class="tombol">
            </div>
            <div class="cek">
				<center>
					<a href="lihat.php" >::Database</a>
				</center>
			</div>
        </form>
    </div>
</div>
</div>
</div>
</section>
	<section id="ABOUT">
		<div class="container">
			<h3>ABOUT</h3><br>
			<p>		Haii..  saya Alviandri Yusuf , biasanya di panggil Alvi. Saya lahir di Bekasi, 24 April 2002, yang berarti tahun ini berusia 20 tahun. Saya tinggal di Komp Polri Munjul, Jakarta Timur, DKIJakarta. Saya dulu bersekolah di SMA Yadika 11 Jatirangga dan sekarang saya sedang melanjutkan Studi saya di Universitas Ahmad Dahlan dengan jurusan Informatika. Saya memliki bebrapa hobi salah satunya yaitu bermain Futsal dan game online. Sekian perkenalan sinkat dari saya. </p>
		</div>
	</section>
	<section id="Image">
			<div class="container">
				<h3>More Image</h3>
				<div class="col-2">
					<a href="">
						<img src="foto.jpg">
					</a>
				</div>

				<div class="col-2">
					<a href="">
						<img src="foto2.jpg">
					</a>
				</div>
			</div>
	</section>

	<section id="contact">
			<div class="container">
				<h3>Contact</h3>
				<div class="box">
					<div class="col-3">
						<h4>Address</h4>
						<p>Komp.Polri Munjul, Rt 09/ Rw 03, Munjul, Cipayung, Jakarta timur, DKIJAKARTA</p>
					</div>
					<div class="col-3">
						<h4>Email</h4>
						<p>alviandriyusuf24@gmail.com</p>
					</div>
					<div class="col-3">
						<h4>Whatshapp</h4>
						<p>081413460807</p>
					</div>
				</div>
			</div>
	</section>
	<section id="MASSAGE">
			<div class="container">
				<h3>Contact US</h3>
				<h4>Untuk Memberikan Pesan dan Saran dapat mengisi form di bawah : </h4>
				<br>
				<div class="cont-1">
			<form action="proses2.php" method="POST" onsubmit="validasiForm()">
            <div>
                <label>
                    Nama Lengkap:
                </label>
                <input type="text" name="name" id="name">
            </div>
            <div>
                <label>
                    Email:
                </label>
                <input type="email" name="mail" id="mail">
            </div>
            <div>
                <label>
                    Perusahaan :
                </label>
                <input type="text" name="company" id="company">
            </div>
             <div>
                <label>
                    Pesan :
                </label>
                <textarea cols="100" rows="5" name="message" id="message"></textarea>
            </div>
            <div>
                <label>
                    Kritik dan saran:
                </label>
                <textarea cols="100" rows="5" name="critic" id="critic">
                </textarea>
            </div>
            <div>
                <input type="submit" value="Login" class="tombol">
            </div>
            <div class="cek">
				<center>
					<a href="lihat2.php" >::Database</a>
				</center>
			</div>
        </form>
				</div>
			</div>
	</section>
	<footer>
		<div class="container">
			<small>Copyright &copy; 2022 - ALVIANDRI YUSUF.</small>
		</div>
	</footer>
	<script type="text/javascript">
    	var tombolMenu = document.getElementsByClassName('tombol-menu')[0];
		var menu = document.getElementsByClassName('menu')[0];
		tombolMenu.onclick = function() {
    	menu.classList.toggle('active');
		}
		menu.onclick = function() {
    	menu.classList.toggle('active');
		}

		function validasi(){
        var nama = document.getElementById("nama").value;
        var email = document.getElementById("email").value;
        var password = document.getElementById("status").value;
        var website = document.getElementById("website").value;
        var alamat = document.getElementById("alamat").value;
        	if (nama != "" && email !="" && status !="" && website !="" && alamat !=""){
            	alert('Anda berhasil menginputkan, selamat masuk ke web saya');
        	}else{
            	alert('Anda harus mengisi data dengan lengkap !');
        		}
    		}

    	function validasiForm(){
        var name = document.getElementById("name").value;
        var mail = document.getElementById("mail").value;
        var company = document.getElementById("company").value;
        var message = document.getElementById("message").value;
        var critic = document.getElementById("critic").value;
        	if (name != "" && mail !="" && company !="" && message !="" && critic !=""){
            	alert('Anda berhasil menginputkan, selamat masuk ke web saya');
        	}else{
            	alert('Anda harus mengisi data dengan lengkap !');
        		}
    		}
    </script>
</body>
</html>